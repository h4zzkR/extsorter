#include <cstdio>
#include <cinttypes>
#include <cstring>
#include <string_view>

template <typename NumT, size_t sz = sizeof(NumT)>
struct BinaryFile {
    std::string_view path;
    FILE* file;

    BinaryFile(const BinaryFile& other) : BinaryFile(other.path) {}
    BinaryFile(BinaryFile&& other) : path(other.path), file(other.file) {
        other.file = nullptr;
    }

    BinaryFile(std::string_view path) : path(path), file(nullptr) {
        file = fopen(path.data(), "rb+");
        if (!file) {
            file = fopen(path.data(), "wb+");
        }
    }

    size_t Read(size_t offsetItems, size_t countItems, NumT* where) {
        if (!file) {
            return 0;
        }

        fseek(file, offsetItems * sz, SEEK_SET);
        size_t bytesRead = fread(where, sz, countItems, file);

        return bytesRead;
    }

    void Write(size_t offsetItems, size_t countItems, const NumT* from) {
        if (!file) {
            return;
        }

        fseek(file, offsetItems * sz, SEEK_SET);
        fwrite(from, sz, countItems, file);
        fflush(file);
    }

    ~BinaryFile() {
        if (file) {
            fclose(file);
        }
    }
};
