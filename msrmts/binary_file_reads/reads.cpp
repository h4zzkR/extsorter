#include "binaryFile.hpp"
#include <iostream>

int main() {
  try {
    std::string_view filePath = "data.bin";
    BinaryFile<int> binaryFile(filePath);

    // Assuming each item in the file is an integer
    const size_t itemsToRead = 10;
    int buffer[itemsToRead];

    size_t itemsRead = binaryFile.Read(0, itemsToRead, buffer);

    std::cout << "Read " << itemsRead << " items from the file:" << std::endl;
    for (size_t i = 0; i < itemsRead; ++i) {
      std::cout << buffer[i] << " ";
    }
    std::cout << std::endl;

  } catch (const std::exception &e) {
    std::cerr << "Exception: " << e.what() << std::endl;
    return 1;
  }

  return 0;
}
