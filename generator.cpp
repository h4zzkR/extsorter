#include <string_view>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <random>

void generate_bin(std::string_view filename, int size) {
  std::fstream data(filename.data(),
                   std::ios::out | std::ios::binary | std::ios::trunc);
  std::default_random_engine generator;
  std::uniform_int_distribution<int> num(std::numeric_limits<int>::min(),
                                        std::numeric_limits<int>::max());

  generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
  for (int i = 0; i < size; ++i) {
    int x = num(generator);
    data.write((char *)&x, sizeof(int));
  }

  data.close();
}

inline size_t mb2bt(size_t mb) { return mb * 1024 * 1024; }

int main(int argc, char* argv[]) {
  if (argc < 3) {
    std::cout << "Usage: data_generator <filepath> <num_ints_to_generate>" << std::endl;
    exit(1);
  }

  std::string_view filepath(argv[1]);
  size_t numInts2Gen = std::stoi(argv[2]);

  std::cout << "=== generating data ===\n";
  generate_bin(filepath, numInts2Gen);
  std::cout << "=== generate finished ===\n";
}
