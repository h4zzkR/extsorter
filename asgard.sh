#!/bin/bash

# GENERATE DATA BEFORE USE:
# `mkdir build && g++ --std=c++20 generator.cpp -o build/generator && ./build/generator data.bin 26214400``
# 1gb   = 1 073 741 824bt = 268435456 ints (file)
# 100mb = 104 857 600bt   = 26214400  ints (ram)
# set ram and mergeBlock constraits in main.cpp

workDir="work"
inputFile="data.bin"
outputFile="build/output.bin"
passToSortFile="work/data.bin"

init() {
  rm -rf build && mkdir build
  rm -rf logs && mkdir logs

  g++ --std=c++20 tester.cpp -o build/tester
  g++ --std=c++20 generator.cpp -o build/generator

  chmod a+x build/tester
  chmod a+x build/generator

  echo "Copying data.bin to work..."
  rm -rf $workDir
  mkdir $workDir
  cp $inputFile $passToSortFile
}

# Display options
echo "Choose an option:"
echo "1. RUN [-t, --test]"
echo "2. ASAN"
echo "3. VALGRIND MASSIF"

# Get the option from the command line
option="$1"

# Check if the user wants to run the test
testMode="false"
if [[ $# -ge 2 ]]; then
  if [[ $2 == "-t" || $2 == "--test" ]]; then
    testMode="true"
  fi
fi

# Call the copyData function before executing each command
init

case $option in
1)
  echo "Running..."
  g++ --std=c++20 main.cpp -o build/sorter && ./build/sorter $passToSortFile $outputFile $workDir &> logs/run.log
  if [[ $testMode == "true" ]]; then
    echo "Running tests..."
    ./build/tester $inputFile $outputFile &> logs/test.log && cat logs/test.log
  fi
  ;;

2)
  echo "Running with AddressSanitizer..."
  g++ -fsanitize=address -g -std=c++20 main.cpp -o build/sorter && ./build/sorter $passToSortFile $outputFile $workDir &> logs/asan.log
  ;;

3)
  echo "Running with Valgrind Massif..."
  g++ -g -std=c++20 main.cpp -o build/sorter && valgrind --tool=massif --massif-out-file=massif_ ./build/sorter $passToSortFile $outputFile $workDir && ms_print massif_ > logs/massif.log && rm -rf massif_
  ;;

*)
  echo "Invalid option"
  ;;
esac
