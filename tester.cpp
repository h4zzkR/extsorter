#include "utility.hpp"
#include <vector>
#include <algorithm>
#include <string_view>
#include <filesystem>
#include <iostream>

inline size_t mb2bt(size_t mb) { return mb * 1024 * 1024; }

int main(int argc, char* argv[]) {
  if (argc < 3) {
    std::cout << "Usage: test_program <filepath> <filepathOut>" << std::endl;
    return 1;
  }

  std::string_view filepath(argv[1]);
  std::string_view filepathOut(argv[2]);

  auto v1 = read_bin(filepath);
  std::sort(v1.begin(), v1.end());
  auto v2 = read_bin(filepathOut);

  assert(v1.size() == v2.size());
  isVectorSorted(v2, filepathOut);

  std::cout << "TEST OK\n";

  return 0;
}
