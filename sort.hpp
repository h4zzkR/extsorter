#include <algorithm>
#include <cassert>
#include <cstddef>
#include <filesystem>
#include <string_view>
#include <vector>
#include <iostream>

#include "logger.hpp"
#include "utility.hpp"
#include "binary_file.hpp"
#include "filesystem.hpp"
#include "defines.hpp"

template <typename NumT = int>
class Sorter {

  static constexpr size_t sz = sizeof(NumT);
  static constexpr size_t kThreadStackSizeMb = 8;
  static constexpr size_t kExtraMemMb = 0; // TODO: set real number
  static constexpr size_t kMb2Bt = 1024 * 1024;

  class IO {
  public:
    IO() = default;
    IO(fs::path ioA, fs::path ioB): _ioA(ioA.c_str()), _ioB(ioB.c_str()) {};

    void Switch() {
      modifier = !modifier;
    }

    BinaryFile<NumT>& Out() {
      if (!modifier) {
        return _ioA;
      } else {
        return _ioB;
      }
    }

    BinaryFile<NumT>& Inp() {
      if (modifier) {
        return _ioA;
      } else {
        return _ioB;
      }
    }

  private:
    BinaryFile<NumT> _ioA;
    BinaryFile<NumT> _ioB;
    bool modifier = true;
  };

  struct Block {
    Block() = default;

    Block(size_t begin, size_t size, IO* io): _begin(begin), _size(size), _io(io) {}

    size_t Read(size_t offsetInBlockItems, size_t countItems, NumT* where) {
      return _io->Inp().Read(_begin + offsetInBlockItems, countItems, where);
    }

    size_t ReadWritten(size_t offsetInBlockItems, size_t countItems, NumT* where) {
      return _io->Out().Read(_begin + offsetInBlockItems, countItems, where);
    }

    void Write(size_t offsetInBlockItems, size_t countItems, const NumT* from) {
      _io->Out().Write(_begin + offsetInBlockItems, countItems, from);
    }

    size_t Size() {
      return _size;
    }

    void SetSize(size_t size) {
      _size = size;
    }

    void SetBegin(size_t begin) {
      _begin = begin;
    }

    size_t& Iter() {
      return _iter;
    }

  private:
    
    size_t _begin = 0;
    size_t _size = 0;
    size_t _iter = 0;
    IO* _io = nullptr;
  };

  class BlockGroup {
  public:
    BlockGroup() = default;

    BlockGroup(size_t groupSize, size_t blockSizeItems, IO& io): size(groupSize), blocks(groupSize) {
      size_t begin = 0;
      for (size_t i = 0; i != groupSize - 1; ++i, begin += blockSizeItems) {
        blocks.data[i] = Block(begin, blockSizeItems, &io);
      }
      auto lastBlockSize = ceilDiv(io.Inp().Size() - (groupSize - 1) * blockSizeItems * sz, sz);
      blocks.data[groupSize - 1] = Block(begin, lastBlockSize, &io);
    }

    Block& Get(size_t idx) { return blocks.data[idx]; }
    size_t Size() { return size; }
    void SetSize(size_t size) { this->size = size;}

  private:
    size_t size;
    Arena<Block> blocks; // less RAM => more blocks => larger blocks Arena
  };

  struct MergerData {
    Arena<NumT> output;
    Arena<NumT> buffers;

    size_t mergeBufLimit = 0;
    size_t sessionIter = 0;
  };

  void blockSort(size_t recursionsToRead, size_t blockSize) {
    auto arena = Arena<NumT>(blockSize);
    _group = BlockGroup(recursionsToRead, blockSize, _io);

    logger.log("=== BlockSort: size={} blocks | size={} bytes ===", recursionsToRead, recursionsToRead * sizeof(Block));

    for (size_t i = 0; i < recursionsToRead; ++i) {
      auto read = _io.Inp().Read(blockSize * i, blockSize, arena.data.data());

      logger.log("blockSort: read={} ints", read);

      std::sort(arena.data.begin(), arena.data.end());

      auto& block = _group.Get(i);
      block.SetSize(read);
      block.Write(0, read, arena.data.data());

#ifdef DEBUG
      std::vector<int> buf; buf.resize(read);
      block.ReadWritten(0, read, buf.data());
      isVectorSorted(buf, "block number " + (i - '0'));
#endif
    }

    _io.Switch();
  }

  size_t mergePackOfBlocks(size_t blockFirst, size_t blockFinal, size_t outputOffset) {
    size_t kMerges = blockFinal - blockFirst;

    if (kMerges == 0) {
      return 0;
    }
    //  else if (kMerges == 1) {

    //   // TODO: fast copy
    // }

    size_t outputIter = 0;
    size_t flushesCnt = 0;
    size_t outputSize = _mergeData.output.data.size();

    auto readMore = [this, blockFirst, blockFinal, outputSize](size_t blockId) {
      size_t offs = (blockId - blockFirst) * _mergeData.mergeBufLimit;
      auto bufPos = _mergeData.buffers.data.data() + offs;
      auto& block = _group.Get(blockId);
      auto read = block.Read(block.Iter(), _mergeData.mergeBufLimit, bufPos);
      logger.log("mergePackOfBlocks: read={} ints | block={} | blockIndex={} | "
                 "blockSize={}",
                 read, blockId, block.Iter(), block.Size());
      if (offs + read > outputSize) {
        logger.log("{} offs, {} read, {} size, {} mergeBufLimit", offs, read, outputSize, _mergeData.mergeBufLimit);
        assert(false);
      }
    };

    // fill buffers first
    for (size_t i = blockFirst; i < blockFinal; ++i) {
      readMore(i);
    }

    while (true) { // build new block from the old ones
      NumT minValue = std::numeric_limits<NumT>::max();
      size_t minBlockIdx = std::numeric_limits<size_t>::max();

      // search min:
      for (size_t i = blockFirst; i < blockFinal; ++i) {
        auto& block = _group.Get(i);
        auto iter = block.Iter();
        auto blockSize = block.Size();

        if (iter >= blockSize) {
          continue; // block is done
        }

        auto bufIter = (i - blockFirst) * _mergeData.mergeBufLimit + iter % _mergeData.mergeBufLimit;
        auto item = _mergeData.buffers.data[bufIter];

        if (item < minValue) {
          minValue = item;
          minBlockIdx = i;
        }
      }

      if (minBlockIdx == std::numeric_limits<size_t>::max()) {
        _io.Out().Write(outputOffset + flushesCnt * outputSize, outputIter,  _mergeData.output.data.data());
        return flushesCnt * outputSize + outputIter; // all are done.
      }

      _mergeData.output.data[outputIter++] = minValue;
      auto newIdx = ++_group.Get(minBlockIdx).Iter();
      auto blockSize = _group.Get(minBlockIdx).Size();

      if (newIdx != blockSize && newIdx % _mergeData.mergeBufLimit == 0) { // read new values from old block
        readMore(minBlockIdx);
      }

      if (outputIter == outputSize) { // flush merged to file
        _io.Out().Write(outputOffset + flushesCnt * outputSize, outputIter,  _mergeData.output.data.data());
        outputIter = 0;
        flushesCnt++;
      }
    }
  }

  void merge() { // TODO: make output larger, smart formula
    size_t numPartitions = _maxBufferBlockSize /  _mergeBufBlockLimit;
    size_t readBuffersSize = numPartitions / 2 * _mergeBufBlockLimit;
    size_t flushBufferSize = ceilDiv(numPartitions, 2) * _mergeBufBlockLimit;
    size_t kMerges = numPartitions / 2;

    if (numPartitions <= 1) {
      throw std::runtime_error("Wrong configuration: not enough memory to merge and flush at least two blocks");
    }

    if (readBuffersSize > flushBufferSize) {
      throw std::runtime_error("Wrong configuration: merge buffer is larger than flush buffer - cannot flush");
    }

    if (flushBufferSize < 10) {
      logger.log("WARN: merge output buffer is too small for efficient flush");
    }

    _mergeData.output = std::move(Arena<NumT>(flushBufferSize));
    _mergeData.buffers = std::move(Arena<NumT>(readBuffersSize));

    logger.log("=== MERGE START: _mergeBufBlockLimit={} ints | "
               "_maxBufferBlockSize={} ints | flushBufSize={} ints | readBufsSize={} ints ===",
               _mergeBufBlockLimit, _maxBufferBlockSize, flushBufferSize, readBuffersSize);

    while (true) { // process all blocks in previous session directory:
      ++_mergeData.sessionIter;

      // explore new:
      for (size_t bI = 0; bI < _group.Size(); ++bI) {
        logger.log("name=block{}, sz={}", bI, _group.Get(bI).Size());

#ifdef DEBUG
      auto& block = _group.Get(bI);
      std::vector<int> buf; buf.resize(block.Size());
      block.Read(0, block.Size(), buf.data());
      isVectorSorted(buf, "block number " + (bI - '0'));
#endif
      }

      auto sessionBlocksCnt = _group.Size();

      if (sessionBlocksCnt == 1) {
        logger.log("{}", _io.Inp().path);
        std::filesystem::rename(_io.Inp().path, _output);
        break; // merged all blocks, sort is done.
      }

      // optimize size to fit more:
      // _mergeData.mergeBufLimit = std::max(_mergeBufBlockLimit, bufferSize / sessionBlocksCnt);

      _mergeData.mergeBufLimit = _mergeBufBlockLimit;
      kMerges = std::min(kMerges, sessionBlocksCnt);

      logger.log("=== STARTING NEW session={} | kMerges={} | mergeLimit={} | blocks={} ===",
                 _mergeData.sessionIter, kMerges, _mergeData.mergeBufLimit, sessionBlocksCnt);

      size_t mergePackName = 0;
      size_t outputOffset  = 0;

      for (size_t blockFirst = 0, blockLast = kMerges;
           blockFirst < sessionBlocksCnt; blockFirst += kMerges,
                  blockLast = std::min(blockLast + kMerges, sessionBlocksCnt)) {

        logger.log("=== session={} | blockFirst={} | blocksLast={} | "
                   "newBlockName={} ===",
                   _mergeData.sessionIter, blockFirst, blockLast,
                   mergePackName);

        logger.log("Merging to: file={}", mergePackName);


        size_t numWritten = mergePackOfBlocks(blockFirst, blockLast, outputOffset);
        _group.Get(blockFirst).Iter() = 0;
        _group.Get(blockFirst).SetSize(numWritten);

        outputOffset += numWritten;
        mergePackName++;
      }

      size_t beginSum = 0;
      for (size_t blockFirst = 0, iter = 0; blockFirst < sessionBlocksCnt; blockFirst += kMerges, ++iter) {
        _group.Get(iter) = std::move(_group.Get(blockFirst));
        _group.Get(iter).SetBegin(beginSum);
        beginSum += _group.Get(iter).Size();
      }
      _group.SetSize(ceilDiv(_group.Size(), kMerges));

      _io.Switch();
    }
  }

public:
  void sort(bool _merge = true) {

    // Phase I.

    // For some reason std::sort may throw std::bad_alloc.
    // Seems there is no guarantee that std::sort will not allocate O(n) extra memory?
    // auto blockSize = ceilDiv(_maxBufferBlockSize, 2);

    auto blockSize = _maxBufferBlockSize;
    size_t recursionsToRead = ceilDiv(_fileSize, blockSize);

    logger.log(
        "blockSort init: _fileSize={}, recursionsToRead={}, blockSize={}",
        _fileSize, recursionsToRead, blockSize);

    blockSort(recursionsToRead, blockSize);

    logger.log("blockSort is done");
    
    // Phase II. Merge
    if (_merge) {
      merge();
    }
  }

  Sorter(fs::path input, fs::path output, fs::path workDir,
         size_t ramPoolLimitBt, size_t mergeBufBlockLimitBt,
         size_t numThreads = 0): _workDir(workDir),
                                 _output(output), _io(input, _workDir / "ioB.bin") {

    // if (numThreads * kThreadStackSizeMb * kMb2Bt > _ramPoolLimitBt) {
    //   throw std::bad_alloc();
    // }

    const_cast<size_t&>(_ramPoolLimitBt) = ramPoolLimitBt;
    const_cast<size_t&>(_mergeBufBlockLimit) = ceilDiv(mergeBufBlockLimitBt, sz);
    const_cast<size_t&>(_fileSize) = ceilDiv(fs::file_size(input), sz);
    const_cast<size_t&>(_maxBufferBlockSize) =
        ceilDiv(_ramPoolLimitBt - kExtraMemMb * kMb2Bt -numThreads * kThreadStackSizeMb * kMb2Bt, sz);

    logger.log("initial: _maxBufferBlockSize={} ints | _mergeBufBlockLimit={} "
               "ints | fileSize={} ints",
               _maxBufferBlockSize, _mergeBufBlockLimit, _fileSize);
  }

  ~Sorter() { 
    // fs::remove_all(_workDir); // fs drains heap, so leave it
  }

private:
  const size_t _ramPoolLimitBt = 0;
  const size_t _mergeBufBlockLimit = 0;
  const size_t _maxBufferBlockSize = 0;
  const size_t _fileSize = 0;

  fs::path _workDir;
  fs::path _output;

  IO _io;
  BlockGroup _group;
  MergerData _mergeData;

  size_t _numRamBlocksPerFile = 0;
  size_t _numFiles = 0;

  Logger logger{std::cerr};
};