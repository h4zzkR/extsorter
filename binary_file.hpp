#include <cstdio>
#include <cinttypes>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <string_view>
#include <string>

template <typename NumT, size_t sz = sizeof(NumT)>
struct BinaryFile {
    int fileDescriptor = -1;
    std::string path = ""; // TODO: get rid of

    BinaryFile() = default;
    BinaryFile(const BinaryFile& other) = delete; // Prevent copying for simplicity
    BinaryFile(BinaryFile&& other) : fileDescriptor(other.fileDescriptor), path(other.path) {
        other.fileDescriptor = -1;
    }

    BinaryFile(std::string_view path) : fileDescriptor(-1), path(path) {
        fileDescriptor = open(path.data(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
        if (fileDescriptor == -1) {
            fprintf(stdout, "%s\n", path.data());
            perror("Error opening file");
        }
    }

    size_t Size() const {
        struct stat st;
        if (fstat(fileDescriptor, &st) == -1) {
            perror("Error getting file size");
            return 0;
        }
        return st.st_size;
    }

    size_t Read(size_t offsetItems, size_t countItems, NumT* where) const {
        if (fileDescriptor == -1) {
            return 0;
        }

        off_t offset = offsetItems * sz;
        ssize_t bytesRead = pread(fileDescriptor, where, sz * countItems, offset);

        if (bytesRead == -1) {
            perror("Error reading from file");
            return 0;
        }

        return bytesRead / sz;
    }

    void Write(size_t offsetItems, size_t countItems, const NumT* from) {
        if (fileDescriptor == -1) {
            return;
        }

        off_t offset = offsetItems * sz;
        ssize_t bytesWritten = pwrite(fileDescriptor, from, sz * countItems, offset);

        if (bytesWritten == -1) {
            perror("Error writing to file");
        }
    }

    ~BinaryFile() {
        if (fileDescriptor != -1) {
            close(fileDescriptor);
        }
    }
};
