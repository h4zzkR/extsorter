#include <filesystem>
#include <format>
#include <fstream>
#include <string_view>

#include "defines.hpp"

#ifdef LOGGING
class Logger
{
    std::ofstream file = {};
    std::ostream& stream;

public:
    explicit Logger(std::ostream& os)
        : stream{os}
    {}
    explicit Logger(std::filesystem::path& filename)
        : file{filename, std::ios::app},
          stream{file}
    {}

    // This one has compile-time checking
    template<typename... Args>
    bool log(std::format_string<Args...> format, Args&&... args)
    {
        try {
            stream << std::format(format, std::forward<Args>(args)...) + "\n"
                   << std::flush;
            return true;
        } catch (...) {
            return false;
        }
    }

    // Run-time checking only
    template<typename... Args>
    bool log_unchecked(std::string_view format, Args&&... args)
    {
        try {
            stream << std::vformat(format, std::make_format_args(args...)) + "\n"
                   << std::flush;
            return true;
        } catch (...) {
            return false;
        }
    }
};
#else
class Logger
{
public:
    explicit Logger(std::ostream&){}
    explicit Logger(std::filesystem::path&){}
    template<typename... Args>
    bool log(std::format_string<Args...>, Args&&...) { return true; }
    template<typename... Args>
    bool log_unchecked(std::string_view, Args&&...) { return true; }
};
#endif
// #include <stdio.h>

// #define LOG(...) fprintf(stdout, __VA_ARGS__); fflush(stdout)