#include "sort.hpp"
#include <string_view>
#include <filesystem>

inline size_t mb2bt(size_t mb) { return mb * 1024 * 1024; }

int main(int argc, char* argv[]) {
  if (argc < 4) {
    std::cerr << "Usage: " << argv[0] << " <filepath> <filepathOut> <workpath>" << std::endl;
    return 1;
  }

  std::string_view filepath(argv[1]);
  std::string_view filepathOut(argv[2]);
  std::string_view workpath(argv[3]);

  size_t ram = 26214400 * sizeof(int);
  size_t mergeBlockSize = 25600 * sizeof(int);

  auto sort = Sorter<int>(filepath, filepathOut, workpath, ram, mergeBlockSize, 0);
  sort.sort();

  return 0;
}
