#include <span>
#include <vector>
#include <string>
#include <iostream>
#include <cassert>

#include <fstream>

template <typename T> struct Arena {
private:
  T *_memory = nullptr;

public:
  Arena() = default;

  Arena(std::size_t sizeInItems) {
    _memory = new T[sizeInItems];
    data = std::span<T>(_memory, sizeInItems);
  }

  Arena(Arena &&oth) : _memory(oth._memory), data(std::move(oth.data)) {
    oth._memory = nullptr;
  }

  Arena &operator=(Arena &&oth) {
    _memory = oth._memory;
    data = std::move(oth.data);
    oth._memory = nullptr;
    return *this;
  }

  Arena &operator=(const Arena &oth) = delete;

  Arena(const Arena &oth) = delete;

  ~Arena() {
    if (_memory) {
      delete[] _memory;
    }
  }

  std::span<T> data{};
};


inline size_t ceilDiv(size_t a, size_t b) { return 1 + ((a - 1) / b); }

std::vector<int> read_bin(std::string_view filename) {
  std::ifstream input(filename.data(), std::ios::binary);

  // Determine the size of the file
  input.seekg(0, std::ios::end);
  std::streampos fileSize = input.tellg();
  input.seekg(0, std::ios::beg);

  // Calculate the number of integers in the file
  size_t numIntegers = fileSize / sizeof(int);

  // Read the file content into a vector of ints
  std::vector<int> data(numIntegers);
  input.read(reinterpret_cast<char *>(data.data()), fileSize);

  // Close the file
  input.close();
  return data;
}

void isVectorSorted(const std::vector<int> &v, std::string_view path) {
  for (size_t i = 1; i < v.size(); ++i) {
    if (v[i] < v[i - 1]) {
      std::cout << "FATAL: " << v[i] << " < " << v[i - 1] << " in " << path.data()
                << std::endl;

      for (auto &it : v) {
        std::cout << it << ' ';
      }
      std::cout << std::endl;

      assert(false);
    }
  }
}
